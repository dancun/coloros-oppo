##########################################################################################
#
# Magisk Module Template Config Script
#
##########################################################################################
##########################################################################################
#
# Instructions:
#
# 1. Place your files into system folder (delete the placeholder file)
# 2. Fill in your module's info into module.prop
# 3. Configure the settings in this file (config.sh)
# 4. If you need boot scripts, add them into common/post-fs-data.sh or common/service.sh
# 5. Add your additional or modified system properties into common/system.prop
#
##########################################################################################

##########################################################################################
# Configs
##########################################################################################

# Set to true if you need to enable Magic Mount
# Most mods would like it to be enabled
SKIPMOUNT=false
#是否安装模块后自动关闭，改为faslse，安装后不会自动勾选启用

# Set to true if you need to load system.prop
PROPFILE=false
#是否使用common/system.prop文件

# Set to true if you need post-fs-data script
POSTFSDATA=true
#是否使用post-fs-data脚本执行文件

# Set to true if you need late_start service script
LATESTARTSERVICE=false
#是否在开机时候允许允许common/service.sh中脚本

##########################################################################################
# Installation Message
##########################################################################################

##########################################################################################
# Replace list
##########################################################################################



# List all directories you want to directly replace in the system
# Check the documentations for more info about how Magic Mount works, and why you need this
[ "$(getprop ro.product.vendor.model)" != "PGEM10" ] && abort "- 不支持的机型！"
if [ "$KSU" != "true" ]
then
# This is an example
REPLACE="
/system/system_ext/etc/horae/horae_SM8550.conf
"
mkdir -p $MODPATH/odm/firmware/fastchg/21131
touch $MODPATH/odm/firmware/fastchg/21131/charging_thermal_config_default.txt
mkdir -p $MODPATH/system_ext/etc/horae
touch $MODPATH/system_ext/etc/horae/horae_SM8550.conf
mkdir -p $MODPATH/my_product/etc/extension
touch $MODPATH/my_product/etc/extension/feature_com.android.launcher_premium.xml
cp -r /my_product/vendor/etc $MODPATH/my_product/vendor

cat > $MODPATH/post-fs-data.sh << 'EOF'
mount -o bind ${0%/*}/my_product/etc/extension/com.oplus.oplus-feature.xml /my_product/etc/extension/com.oplus.oplus-feature.xml
mount -o bind ${0%/*}/my_product/etc/extension/com.oplus.app-features.xml /my_product/etc/extension/com.oplus.app-features.xml
mount -o bind ${0%/*}/my_product/etc/extension/feature_com.android.launcher_premium.xml /my_product/etc/extension/feature_com.android.launcher_premium.xml
mount -o bind ${0%/*}/my_product/etc/apns-conf.xml /my_product/etc/apns-conf.xml
mount -o bind ${0%/*}/my_product/etc/oplus_vrr_config.json /my_product/etc/oplus_vrr_config.json
mount -o bind ${0%/*}/my_product/vendor/etc /my_product/vendor/etc
mount -o bind ${0%/*}/my_product/etc/vibrator_service_config /my_product/etc/vibrator_service_config
mount -o bind ${0%/*}/my_product/etc/vibrator /my_product/etc/vibrator
mount -o bind ${0%/*}/odm/etc/temperature_profile /odm/etc/temperature_profile 
mount -o bind ${0%/*}/odm/etc/ThermalServiceConfig /odm/etc/ThermalServiceConfig
EOF
else
# Construct your own list here, it will override the example above
# !DO NOT! remove this if you don't need to replace anything, leave it empty as it is now
REMOVE="
/my_product/etc/extension/feature_com.android.launcher_premium.xml
/odm/firmware/fastchg/21131/charging_thermal_config_default.txt
/system_ext/etc/horae/horae_SM8550.conf
"
echo 'mount -t overlay -o lowerdir=${0%/*}/my_product:/mnt/vendor/my_product overlay /mnt/vendor/my_product' > $MODPATH/post-fs-data.sh
fi
#添加您要精简的APP/文件夹目录
#例如：精简状态栏，找到状态栏目录为  /system/priv-app/SystemUI/SystemUI.apk     
#转化加入:/system/priv-app/SystemUI
#（可以搭配高级设置获取APP目录）

##########################################################################################
# Permissions
##########################################################################################
#释放文件，普通shell命令
#应用动画速度调节
mkdir -p $MODPATH/my_product/etc/extension
cp /my_product/etc/extension/com.oplus.app-features.xml $MODPATH/my_product/etc/extension/com.oplus.app-features.xml
sed -i '/DEVICE_LEVEL_HIGH/d' $MODPATH/my_product/etc/extension/com.oplus.app-features.xml

#解除默认接入点修改限制
cp /my_product/etc/apns-conf.xml $MODPATH/my_product/etc
sed -i '/read_only/s/true/false/g' $MODPATH/my_product/etc/apns-conf.xml

#添加电池优化白名单
mkdir -p $MODPATH/system_ext/oplus
cp /system_ext/oplus/sys_deviceidle_whitelist.xml $MODPATH/system_ext/oplus/sys_deviceidle_whitelist.xml
for item in `pm list packages -3`
do
dumpsys deviceidle whitelist +${item:8} >/dev/null
cmd appops set ${item:8} RUN_IN_BACKGROUND allow
cmd appops set ${item:8} RUN_ANY_IN_BACKGROUND allow
if [[ -z "$(grep -w ${item:8} $MODPATH/system_ext/oplus/sys_deviceidle_whitelist.xml)" ]]
then
sed -i '/<\/filter-conf>/i\    <wl>'"${item:8}"'<\/wl>' $MODPATH/system_ext/oplus/sys_deviceidle_whitelist.xml
fi
done

#特性文件修改
temp_xml=$MODPATH/my_product/etc/extension/com.oplus.oplus-feature.xml
feature=$MODPATH/my_product/etc/extension/feature.txt
# 复制原始XML文件到临时文件
cp /my_product/etc/extension/com.oplus.oplus-feature.xml "$temp_xml"

lines=$(wc -l < "$feature")
i=1
# 从最后一行开始读取insert_lines.txt文件，并循环插入到XML文件中
while [ $i -lt $((lines+2)) ]; do
    # 使用tail和head组合来获取当前行
    line=$(tail -n $i "$feature" | head -n 1)
    # 使用sed命令在临时文件的根节点下插入新行
    # 注意：这里假设<root>节点只出现一次，并且在文件的第一行就开始
    sed -i '/<oplus-config>/a\    '"$line" "$temp_xml"
    i=$((i + 1))
done
rm -rf "$feature"

#最大亮度
cp /my_product/vendor/etc/display_apollo_list.xml $MODPATH/my_product/vendor/etc/display_apollo_list.xml
cp /my_product/vendor/etc/display_brightness_config_samsung1024.xml $MODPATH/my_product/vendor/etc/display_brightness_config_samsung1024.xml
cp /my_product/vendor/etc/display_brightness_config_samsung1500.xml $MODPATH/my_product/vendor/etc/display_brightness_config_samsung1500.xml
sed -i '/range/s/8191/10239/' $MODPATH/my_product/vendor/etc/display_apollo_list.xml
sed -i '/max/s/8191/10239/' $MODPATH/my_product/vendor/etc/display_brightness_config_samsung1024.xml
sed -i '/max/s/8191/10239/' $MODPATH/my_product/vendor/etc/display_brightness_config_samsung1500.xml

#禁用avb2.0校检
$MODPATH/system/bin/avbctl disable-verity --force >/dev/null && ui_print "- 已关闭dm校检" || abort "- 关闭dm校检失败！"
$MODPATH/system/bin/avbctl disable-verification --force >/dev/null && ui_print "- 已关闭启动校检" || abort "- 关闭启动校检失败！"

#解包dtbo镜像
if [ ! -e /data/local/dtbo_$(getprop ro.build.display.ota).img ]
then
rm -f /data/local/dtbo*.img
cat /dev/block/by-name/dtbo$(getprop ro.boot.slot_suffix) > /data/local/dtbo_$(getprop ro.build.display.ota).img
fi
MODTMP=$MODPATH/tmp
mkdir -p $MODTMP/out && mv -f $MODPATH/dtc $MODTMP/bin && chmod +x $MODTMP/bin/*
$MODTMP/bin/mkdtimg dump /data/local/dtbo_$(getprop ro.build.display.ota).img -b $MODTMP/out/dtb >/dev/null
for i in `find $MODTMP/out/dtb.*`; do
$MODTMP/bin/dtc -I dtb -O dts -@ $i -o $i.dts
rm -f $i
done

#温度相关
grep -rl 'temperature' $MODTMP/out | xargs sed -i -e '/smart_chg/d' -e '/temperature/s/<0x17318>/<0x19a28>/' \
-e '/temperature/s/<0x13880>/<0x15f90>/' \
-e '/temperature/s/<0x130b0>/<0x157c0>/' \
-e '/led_warm_bat_decidegc/s/<.*>/<0x1c2>/' \
-e '/led_high_bat_decidegc/s/<.*>/<0x1d6>/' \
-e '/vooc_temp_bat_normal_decidegc/s/<.*>/<0x1c2>/' \
-e '/vooc_temp_bat_warm_decidegc/s/<.*>/<0x1d6>/' \
-e '/vooc_temp_bat_hot_decidegc/s/<.*>/<0x1f9>/'
#打包dtbo镜像
for i in `find $MODTMP/out/*.dts`; do
$MODTMP/bin/dtc -I dts -O dtb -@ -o ${i%.dts} $i
rm -f $i
done
$MODTMP/bin/mkdtimg create $MODTMP/dtbo_new.img $MODTMP/out/dtb.* >/dev/null
cat $MODTMP/dtbo_new.img /dev/zero > /dev/block/by-name/dtbo$(getprop ro.boot.slot_suffix)

#清理临时目录
rm -rf $MODTMP

# Only some special files require specific permissions
# The default permissions should be good enough for most cases

# Here are some examples for the set_perm functions:

# set_perm_recursive  <dirname>                <owner> <group> <dirpermission> <filepermission> <contexts> (default: u:object_r:system_file:s0)
# set_perm_recursive  $MODPATH/system/lib       0       0       0755            0644

# set_perm  <filename>                         <owner> <group> <permission> <contexts> (default: u:object_r:system_file:s0)
# set_perm  $MODPATH/system/bin/app_process32   0       2000    0755         u:object_r:zygote_exec:s0
# set_perm  $MODPATH/system/bin/dex2oat         0       2000    0755         u:object_r:dex2oat_exec:s0
# set_perm  $MODPATH/system/lib/libart.so       0       0       0644

# The following is default permissions, DO NOT remove
set_perm_recursive  $MODPATH/odm/etc  0  0  0755  0644  u:object_r:vendor_configs_file:s0
set_perm_recursive  $MODPATH/odm/firmware/tp  0  0  0755  0644  u:object_r:vendor_configs_file:s0
set_perm_recursive  $MODPATH/my_product/vendor/etc  0  0  0755  0644  u:object_r:vendor_configs_file:s0
set_perm_recursive  $MODPATH/product/bin/init.oplus.nandswap.sh  0  2000  0755  u:object_r:nandswap_exec:s0
set_perm_recursive  $MODPATH/bin 0 0 0755 755
#设置权限，基本不要去动

[ "$KSU" != "true" ] && mv $MODPATH/system_ext $MODPATH/system/system_ext && mv $MODPATH/product $MODPATH/system/product
##########################################################################################
# Custom Functions
##########################################################################################

# This file (config.sh) will be sourced by the main flash script after util_functions.sh
# If you need custom logic, please add them here as functions, and call these functions in
# update-binary. Refrain from adding code directly into update-binary, as it will make it
# difficult for you to migrate your modules to newer template versions.
# Make update-binary as clean as possible, try to only do function calls in it.

