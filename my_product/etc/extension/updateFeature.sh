#!/bin/bash

# 临时文件，用于存储修改后的XML内容
temp_xml=$MODPATH/my_product/etc/extension/com.oplus.oplus-feature.xml
temp_xml="./com.oplus.oplus-feature.xml"
feature="./feature.txt"
# 复制原始XML文件到临时文件
cp /my_product/etc/extension/com.oplus.oplus-feature.xml "$temp_xml"
lines=$(wc -l < "$feature")
index=$((lines+1))
# 从最后一行开始读取insert_lines.txt文件，并循环插入到XML文件中
while [ $index -gt 0 ]; do
    # 使用tail和head组合来获取当前行
    line=$(head -n $index $feature )
    print $line
    # 使用sed命令在临时文件的根节点下插入新行
    # 注意：这里假设<root>节点只出现一次，并且在文件的第一行就开始
    sed -i '/<oplus-config>/a\    '"$line" "$temp_xml"
    
    # 递减计数器
    index=$((index - 1))
done
# 读取要插入的行，并循环插入到XML文件中
while IFS= read -r line; do
    # 使用sed命令在临时文件的根节点下插入新行
    sed -i '/<oplus-config>/a\    '"$line" "$temp_xml"
done < "$feature"
j=10
for ((i=1; i<=j; i++))
do
echo "$i"
done