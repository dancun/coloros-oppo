while [ "$(getprop sys.boot_completed)" != "1" ]; do
  sleep 3
done

#更新系统后自动禁用校检
avbctl disable-verity --force
avbctl disable-verification --force




#修改温度墙
echo '125000' > ${0%/*}/temp
for temp in /sys/class/thermal/thermal_zone*/trip_point_*_temp
do
if [ "$(cat $temp)" -gt 85000 ]
then
mount -o make,bind ${0%/*}/temp $temp
fi
done

rmmod oplus_ipa_thermal


settings put system peak_refresh_rate 122
settings put system min_refresh_rate 122
echo 4095 >/sys/kernel/oplus_display/max_brightness

#系统更新进程优化
echo $(pgrep update_engine) >/dev/cpuset/top-app/tasks
taskset -p "ff" -a $(pgrep update_engine)
renice -n -20 $(pgrep update_engine)
#挂载FreezerV2(FROZEN) 来自忘川
mkdir /sys/fs/cgroup/frozen/
mkdir /sys/fs/cgroup/unfrozen/
chown system:system /sys/fs/cgroup/frozen/cgroup.procs
chown system:system /sys/fs/cgroup/frozen/cgroup.freeze
chown system:system /sys/fs/cgroup/unfrozen/cgroup.procs
chown system:system /sys/fs/cgroup/unfrozen/cgroup.freeze
echo 1 > /sys/fs/cgroup/frozen/cgroup.freeze
echo 1 > /sys/fs/cgroup/unfrozen/cgroup.freeze

#充电温度模拟 搬运自@int萌新很新
stop horae
stop gameopt_hal_service-1-0
echo "0 36000" > /proc/shell-temp
echo "1 36000" > /proc/shell-temp
echo "2 36000" > /proc/shell-temp
echo 1 > /proc/game_opt/disable_cpufreq_limit
echo 0 > /proc/oplus_scheduler/sched_assist/sched_assist_enabled
echo $(pidof thermal-engine-v2) > /sys/fs/cgroup/frozen/cgroup.procs




#充电守护@int萌新很新
sh ${0%/*}/vooc_ctrl.rc &


