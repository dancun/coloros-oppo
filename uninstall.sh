# 该脚本将在卸载期间执行，您可以编写自定义卸载规则
# 还原镜像
dd if=/data/local/dtbo_$(getprop ro.build.display.ota).img of=/dev/block/by-name/dtbo$(getprop ro.boot.slot_suffix)
rm -f /data/local/*_$(getprop ro.build.display.ota).img
